# Use tidyverse since it already contains some packages
FROM rocker/hadleyverse:latest

# Add here anything else the image would need (ie pandoc,...)
RUN apt-get update \
    && apt-get install -y pandoc

# Copy the repo files into the image
WORKDIR /root
COPY packages_installer.r /root/

# Run the R script to install missing packages
RUN Rscript /root/packages_installer.r

# Clear the files from the image
RUN rm -rf packages_installer.r 