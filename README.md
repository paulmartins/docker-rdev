# docker-rdev

Repository to build and push an R docker image for packages development.

To build the image locally (once you've installed docker)

```{bash}
cd path/to/docker-rdev
docker build -t ppjmartins/rdev .

# check the image is there
docker images

# run the image
docker run --rm -it ppjmartins/rdev:latest /bin/bash

# push the image
docker login --username=ppjmartins
# ENTER PASSWORD
docker push ppjmartins/rdev
```